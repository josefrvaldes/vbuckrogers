#ifndef STATE_H
#define STATE_H

#include <SFML/Graphics.hpp>

using namespace sf;

class State
{
    public:
        State();
        virtual ~State();
        virtual void update() {};
        virtual void render() {};
        enum Estados{ INGAME, GAMEOVER, SUPERADO };

    protected:

    private:
};

#endif // STATE_H
