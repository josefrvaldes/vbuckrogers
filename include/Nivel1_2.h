#ifndef NIVEL1_2_H
#define NIVEL1_2_H

#include <NivelEspacio.h>
#include <SFML/Graphics.hpp>

using namespace sf;

class Nivel1_2 : public NivelEspacio
{
    public:
        Nivel1_2();
        virtual ~Nivel1_2();
        virtual void update();
        virtual void render();

    protected:
        virtual void crearEnemigo();

    private:
};

#endif // NIVEL1_2_H
