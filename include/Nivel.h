#ifndef NIVEL_H
#define NIVEL_H

#include "SFML/Graphics.hpp"
#include <iostream>
#include <Enemigo.h>
#include <Vida.h>

using namespace sf;
using namespace std;

class Nivel
{
    public:
        Nivel();
        virtual ~Nivel();
        virtual void update();
        virtual void render();
        Time *getTiempoTranscurrido() { return tiempo; };
        int getTiempoMaxNivel() { return tiempoMaxNivel; };
        float getPorcentajeTiempoTranscurrido ()
        {
            return 100 * tiempo->asSeconds() / (float)tiempoMaxNivel;
        };

    protected:
        int tiempoMaxNivel; // en segundos
        Clock *reloj;
        Time *tiempo;

        Clock *relojEnemigos;
        bool sumarObjetivoTorreta = true;
        bool sumarObjetivoEnemigo = false;

        int tiempoEntreEnemigos;

        vector<Enemigo *> *enemigos;
        vector<Vida *> *vidas;

        virtual void crearEnemigo();

    private:
};

#endif // NIVEL_H
