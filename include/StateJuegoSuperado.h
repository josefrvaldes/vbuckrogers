#ifndef STATEJUEGOSUPERADO_H
#define STATEJUEGOSUPERADO_H

#include <State.h>
#include "SFML/Graphics.hpp"

class StateJuegoSuperado : public State
{
    public:
        StateJuegoSuperado();
        virtual ~StateJuegoSuperado();
        virtual void render();

    protected:

    private:
        Text *texto;
};

#endif // STATEJUEGOSUPERADO_H
