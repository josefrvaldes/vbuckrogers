#ifndef NIVEL2_2_H
#define NIVEL2_2_H

#include <NivelEspacio.h>

class Nivel2_2 : public NivelEspacio
{
    public:
        Nivel2_2();
        virtual ~Nivel2_2();
        virtual void update();
        virtual void render();

    protected:
        virtual void crearEnemigo();

    private:
};

#endif // NIVEL2_2_H
