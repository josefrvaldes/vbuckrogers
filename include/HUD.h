#ifndef HUD_H
#define HUD_H

#include "SFML/Graphics.hpp"
#include "Nivel.h"
#include <iostream>

using namespace std;
using namespace sf;

class HUD
{
    public:
        HUD(Nivel *nuevoNivel);
        virtual ~HUD();
        void render();
        void update();
        void setNivel(Nivel *nivel);

    protected:

    private:
        const float POS_LBL_TIME_X = 10.f;
        const float POS_LBL_TIME_Y = 10.f;

        const float POS_LBL_SCORE_X = 10.f;
        const float POS_LBL_SCORE_Y = 40.f;

        const float POS_TXT_SCORE_X = 120.f;
        const float POS_TXT_SCORE_Y = 40.f;

        const float POS_MODO_DIOS_X = 10.f;
        const float POS_MODO_DIOS_Y = 80.f;
        const float ESCALA_MODO_DIOS = 0.16f;

        const float POS_BARRA_BLANCA_X = 120.f;
        const float POS_BARRA_BLANCA_Y = 10.f;
        const float HEIGHT_BARRA_BLANCA = 30.f;
        const float WIDTH_BARRA_BLANCA = 380.f;

        const float MARGEN = 5.f;
        const float POS_BARRA_VERDE_X = POS_BARRA_BLANCA_X + MARGEN;
        const float POS_BARRA_VERDE_Y = POS_BARRA_BLANCA_Y + MARGEN;;
        const float HEIGHT_BARRA_VERDE = HEIGHT_BARRA_BLANCA - 2 * MARGEN;
        const float WIDTH_BARRA_VERDE = WIDTH_BARRA_BLANCA - 2 * MARGEN;

        const float POS_INICIO_VIDAS_X = 530.f;
        const float POS_INICIO_VIDAS_Y = 10.f;
        const float ESCALA_SPRITE_VIDAS = 0.32f;

        const float POS_INICIO_OBJETIVOS_X = 530.f;
        const float POS_INICIO_OBJETIVOS_Y = 40.f;
        const float ESCALA_SPRITE_OBJETIVOS = 0.05f;


        Nivel *nivel;

        Text *lblTime;
        Text *lblScore;
        Text *txtScore;
        RectangleShape *barraVidaBlanca;
        RectangleShape *barraVidaVerde;
        Texture *textureVida;
        Texture *textureObjetivo;
        Sprite *spriteModoDios;
        vector<Sprite *> *spritesVidas;
        vector<Sprite *> *spritesObjetivos;

        string getStringFromPuntuacion(int *puntuacion) {
            string aux(to_string(*puntuacion));
            while(aux.length() < 6) {
                aux = "0" + aux;
            }
            return aux;
        };
};

#endif // HUD_H
