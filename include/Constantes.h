#ifndef CONSTANTES_H
#define CONSTANTES_H


class Constantes
{
    public:
        Constantes();
        virtual ~Constantes();
        const static bool DEBUG = false;
        const static int TAMANYO_VENTANA_X = 800;
        const static int TAMANYO_VENTANA_Y = 600;
        const static int PUNTUACION_POR_TORRETA = 1000;
        const static int PUNTUACION_POR_KILL = 100;


    protected:

    private:
};

#endif // CONSTANTES_H
