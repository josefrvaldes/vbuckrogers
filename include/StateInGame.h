#ifndef STATEINGAME_H
#define STATEINGAME_H

#include <State.h>
#include <Jugador.h>
#include <Juego.h>
#include <HUD.h>
#include <Nivel.h>
#include <SFML/Graphics.hpp>

using namespace sf;

class StateInGame : public State
{
    public:
        StateInGame();
        virtual ~StateInGame();
        void update();
        void render();
        void setNivel(Nivel *nuevoNivel);// { nivel = nuevoNivel; }

    protected:

    private:

        Nivel *nivel;
        HUD *hud;


        double posAntX;
        double posAntY;
        double posSigX;
        double posSigY;
};

#endif // STATEINGAME_H
