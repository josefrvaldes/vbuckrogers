#ifndef MOTOR2D_H
#define MOTOR2D_H

#include "SFML/Graphics.hpp"
#include <iostream>

using namespace sf;
using namespace std;

class Motor2D
{
    public:
        Motor2D();
        virtual ~Motor2D();
        static float getValorInterpolado(float xActual, float xMin, float xMax, float yMin, float yMax) {
            return ((xActual - xMin) / (xMax - xMin)) * (yMax - yMin) + yMin;
        };

        static Text* getDefaultText()
        {
            sf::Text *text = new sf::Text();
            text->setPosition(sf::Vector2f(1.0, 1.0));
            sf::Font *font = new sf::Font();
            if(!font->loadFromFile("resources/fonts/arial.ttf")) {
                cout << "No se puede cargar la fuente" << endl;
            }
            text->setCharacterSize(/*24*/32);
            text->setColor(sf::Color::White);
            text->setFont(*font);
            text->setStyle(sf::Text::Bold /*| sf::Text::Underlined*/);

            return text;
        }

    protected:

    private:
};

#endif // MOTOR2D_H
