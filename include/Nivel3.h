#ifndef NIVEL3_H
#define NIVEL3_H

#include <NivelConSuelo.h>

class Nivel3 : public NivelConSuelo
{
    public:
        Nivel3(bool newTorretasConRayos = false);
        virtual ~Nivel3();
        virtual void update();
        virtual void render();

    protected:
        virtual void crearEnemigo();

    private:
};

#endif // NIVEL3_H
