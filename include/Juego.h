#ifndef JUEGO_H
#define JUEGO_H

#include <iostream>
#include <State.h>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class Juego
{
    public:
        static Juego* getInstance();
        void setState(State::Estados tipoEstado);
        State* getState() { return stateActual; };
        void iniciar();
        RenderWindow *getWindow() { return window; }
        View *getView() { return view; }
        Vector2f *getDireccionView() { return direccionView; }
        double *getPercentTick() { return percentTick; }
        bool getModoDios() { return modoDios; }

    protected:

    private:
        string *nombre;
        static Juego *juego;
        State *stateActual;
        RenderWindow *window;
        View *view;
        Vector2f *direccionView;

        Juego();
        virtual ~Juego();
        void buclePrincipal();


        Clock *updateClock;
        Time *timeElapsed;
        double *percentTick;

        bool modoDios = false;

};

#endif // JUEGO_H
