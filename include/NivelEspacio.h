#ifndef NIVELESPACIO_H
#define NIVELESPACIO_H

#include <Nivel.h>
#include <SFML/Graphics.hpp>

using namespace sf;

class NivelEspacio : public Nivel
{
    public:
        NivelEspacio();
        virtual ~NivelEspacio();
        virtual void update();
        virtual void render();

    protected:
        virtual void crearEnemigo();
        bool avanzarNivel = false;
        bool bossLlamado = false;
        Texture *texturaFondo;
        Sprite *spriteFondo;

    private:
};

#endif // NIVELESPACIO_H
