#ifndef NIVELCONSUELO_H
#define NIVELCONSUELO_H

#include "Nivel.h"
#include "Constantes.h"
#include "RectanguloSuelo.h"
#include "Torreta.h"
#include "SpriteParallax.h"
#include "SFML/Graphics.hpp"
#include <iostream>

using namespace sf;
using namespace std;

class NivelConSuelo : public Nivel
{
    public:
        NivelConSuelo(bool torretasConRayos = false);
        virtual ~NivelConSuelo();
        virtual void update();
        virtual void render();

    protected:
        int rSuelo = 200;
        int gSuelo = 160;
        int bSuelo = 68;
        Color *colorRectangulo;
        const float POS_INICIAL_SUELO_X = 0;
        const float POS_INICIAL_SUELO_Y = 180;
        const float TAM_INICIAL_SUELO_X = Constantes::TAMANYO_VENTANA_X;
        const float TAM_INICIAL_SUELO_Y = 10;

        Sprite *spriteSuelo;
        SpriteParallax *spriteFondoBack;
        SpriteParallax *spriteFondoMid;
        SpriteParallax *spriteFondoFront;

        vector<RectanguloSuelo *> *rectangulosSuelo;
        vector<Torreta *> *torretas;

        void inicializarRectangulosSuelo();
        virtual void inicializarTorretas();

        const float velocidadParallaxBack = 2.f;
        const float velocidadParallaxMid = 4.f;
        const float velocidadParallaxFront = 8.f;
        bool torretasConRayos = false;

        virtual void crearEnemigo();

    private:
};

#endif // NIVELCONSUELO_H
