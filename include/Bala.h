#ifndef BALA_H
#define BALA_H

#include "SFML/Graphics.hpp"


using namespace sf;

class Bala
{
    public:
        Bala(Vector2f posicionInicial, Vector2f *vectorDirector);
        virtual ~Bala();
        void update();
        void render();
        Sprite *getSprite(){return spriteBala;};

    protected:

    private:
        Vector2f *vectorDirector;
        Sprite *spriteBala;
        const float VELOCIDAD_BALA = 24.f;
        Vector2f *posActual;
        Vector2f *posSiguiente;
};

#endif // BALA_H
