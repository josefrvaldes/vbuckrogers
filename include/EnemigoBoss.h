#ifndef ENEMIGOBOSS_H
#define ENEMIGOBOSS_H

#include <Enemigo.h>
#include <Bala.h>
#include <SFML/Graphics.hpp>

using namespace sf;

class EnemigoBoss : public Enemigo
{
    public:
        EnemigoBoss(float ySuelo = 0.f);
        virtual ~EnemigoBoss();
        virtual bool update();
        virtual void render();

    protected:
        virtual void inicializarPosYDirInicio(float ySuelo = 0.f);

    private:
        vector<Bala *> *balas;
        void disparar();
        Clock *relojDisparos;
        int milisEntreDisparos = 500;
        bool fase2Iniciada = false;
};

#endif // ENEMIGOBOSS_H
