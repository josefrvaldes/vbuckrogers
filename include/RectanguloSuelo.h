#ifndef RECTANGULOSUELO_H
#define RECTANGULOSUELO_H

#include "SFML/Graphics.hpp"
#include "ElementoSuelo.h"

using namespace sf;

class RectanguloSuelo : public ElementoSuelo
{
    public:
        RectanguloSuelo(float inicioSueloY, int index, int numRectangulosEnPantalla, Color *);
        virtual ~RectanguloSuelo();

        void setRectangulo(RectangleShape *rect) { rectangulo = rect; }

        RectangleShape* getRectangulo() { return rectangulo; }

        virtual void update();
        virtual void render();

    protected:

    private:
        RectangleShape *rectangulo;

        int TAMANYO_MIN = 0;
        int TAMANYO_MAX;
        float getTamanyoInterpolado(float nuevaY);
};

#endif // RECTANGULOSUELO_H
