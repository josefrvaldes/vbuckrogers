#ifndef TORRETARAYOS_H
#define TORRETARAYOS_H

#include <Torreta.h>
#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;
using namespace std;

class TorretaRayos : public Torreta
{
    public:
        //TorretaRayos(float nuevoInicioSuelo);
        TorretaRayos(float posX, float nuevoInicioSuelo);
        virtual ~TorretaRayos();
        virtual bool update(bool reinicio = false);
        virtual void render();
        virtual bool chocaConJugador();


    protected:

    private:
        const int ALTO_TEXTURA = 511;
        const int ANCHO_TEXTURA = 125;
        const int MILISEGUNDOS_TEXTURA = 250;
        const int NUM_TEXTURAS = 8;
        int textActual = 0;
        Sprite *spriteRayosIzq;
        Sprite *spriteRayosDch;
        Texture *texturaRayos;
        vector<IntRect *> *rectangulos;
        Clock *relojTextura;
};

#endif // TORRETARAYOS_H
