#ifndef ENEMIGO_H
#define ENEMIGO_H

#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;
using namespace std;

#include <SFML/Graphics.hpp>
#include <ElementoPintable.h>

using namespace sf;

class Enemigo : public ElementoPintable
{
    public:
        Enemigo(float ySuelo = 0.f);
        virtual ~Enemigo();
        virtual bool update();
        virtual void render();
        virtual Sprite *getSprite() { return sprite; };
        void restarVida() { vida--; };
        int getVida() { return vida; };
        int getPuntuacion() { return puntuacion; };

    protected:
        int vida = 1;
        int puntuacion = 50;
        Texture *textura;
        Sprite *sprite;
        Vector2f *posActual;
        Vector2f *posSiguiente;
        virtual void inicializarPosYDirInicio(float ySuelo = 0.f);

        vector<IntRect *> *rectangulosDisponibles;

        float xOrigen;
        float yOrigen;

        float xFin;
        float yFin;

        float velocidad;

        float ESCALA_MIN = 0.5f;
        float ESCALA_MAX = 1.0f;

        Vector2f *vectorDirector;

    private:

};

#endif // ENEMIGO_H
