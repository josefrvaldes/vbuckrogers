#ifndef VIDA_H
#define VIDA_H

#include <ElementoSuelo.h>

class Vida : public ElementoSuelo
{
    public:
        Vida(float inicioSueloY, float x, float y);
        virtual ~Vida();
        virtual void render();
        virtual bool update();
        virtual Sprite *getSprite() { return sprite; };

    protected:

    private:
        Texture *textura;
        Sprite *sprite;

        const float ESCALA_INICIAL = 0.1;
        const float ESCALA_FINAL = 0.2;
};

#endif // VIDA_H
