#ifndef TORRETA_H
#define TORRETA_H

#include "SFML/Graphics.hpp"
#include "ElementoSuelo.h"

using namespace sf;

class Torreta : public ElementoSuelo
{
    public:
        Torreta(float nuevoInicioSuelo);
        Torreta(float posX, float nuevoInicioSuelo);
        virtual ~Torreta();
        virtual bool update(bool reinicio = false);
        virtual void render();
        Sprite *getSpriteIzq() {return spriteIzq;};
        Sprite *getSpriteDch() {return spriteDch;};
        bool getTorretaChocada() {return torretaChocada;};
        bool getTorretaConeguida() {return torretaConseguida;};
        void chocar() { torretaChocada = true;};
        void conseguir() {torretaConseguida = true;};
        virtual Sprite *getSprite();// { return spriteIzq; };
        virtual bool chocaConJugador();
        virtual bool seConsigueTorreta();

    protected:

        bool torretaConseguida;
        bool torretaChocada;
        Sprite *spriteIzq;
        Sprite *spriteDch;
        Vector2f *getEscalaInterpolada(float nuevaY);
        int getSeparacionInterpolada(float nuevaY);

        const float SEPARACION_INICIAL = 160;
        const float SEPARACION_FINAL = 350;

        const float ESCALA_INICIAL_X = 0.6;
        const float ESCALA_FINAL_X = 1.2;

        const float ESCALA_INICIAL_Y = 1.0;
        const float ESCALA_FINAL_Y = 2.0;


    private:
};

#endif // TORRETA_H
