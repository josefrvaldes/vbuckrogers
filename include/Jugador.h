#ifndef JUGADOR_H
#define JUGADOR_H

#include <SFML/Graphics.hpp>
#include <Constantes.h>
#include <Bala.h>
#include <Vida.h>
#include <Enemigo.h>
#include <iostream>

using namespace sf;
using namespace std;

class Jugador
{
    public:
        virtual ~Jugador();
        void update();
        void render();
        float static getVelocidad() { return velocidadZ; };
        Vector2f *getVectorDirectorOtros() { return vectorDirectorOtros; };
        Sprite *getSprite() { return sprite; };
        float getPosYSombra() { return POS_Y_SOMBRA; };
        int getVidas() { return vidas; };
        int getSalud() { return salud; };
        int getNumObjetivos() { return numObjetivos; };
        void setNumObjetivos(int nuevoNum) { numObjetivos = nuevoNum; };
        int *getPuntuacion() { return puntuacion; };
        static Jugador *getInstance();
        void objetivoConseguido();
        void perderVida();
        void ganarVida();
        void comprobarBalasContraEnemigos(vector<Enemigo *> *, bool sumarObjetivo, vector<Vida *> *vidas);
        void setPintarSombra(bool p) { pintarSombra = p; };

    protected:

    private:
        Jugador();

        static Jugador *jugador;


        Sprite *sprite;
        Sprite *spriteSombra;

        const float velocidad = 16.f;
        const float ESCALA_SOMBRA_MAX =  1;
        const float ESCALA_SOMBRA_MIN =  0.5;
        const float GRADOS_GIRO = 30;
        const float POS_Y_SOMBRA = 540;
        const float POS_MIN_Y = 400;
        const float POS_MAX_Y = 440;
        const float POS_INICIAL_X = Constantes::TAMANYO_VENTANA_X / 2 - 16;
        const float POS_INICIAL_Y = 440.f;
        const float MARGEN_LATERAL = 128.f;
        const int PORCENTAJE_VIDA = 20;

        const int VEL_MIN_Z = 3;
        const int VEL_MAX_Z = 6;

        const int TIEMPO_ENTRE_BALAS = 200;

        Vector2f *vectorDirector;
        Vector2f *vectorDirectorOtros;

        Vector2f *posActual;
        Vector2f *posSiguiente;

        float rotacionActual = 0.f;
        float rotacionSiguiente = 0.f;
        vector<Bala *> *balas;

        void mover();
        void disparar(Vector2f *);
        float getEscalaSombra();

        int vidas;
        int salud;
        int numObjetivos;
        int *puntuacion;

        Clock *relojCadencia;

        static float velocidadZ;
        bool pintarSombra = true;
};


#endif // JUGADOR_H
