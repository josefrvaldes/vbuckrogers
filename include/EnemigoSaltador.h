#ifndef ENEMIGOSALTADOR_H
#define ENEMIGOSALTADOR_H

#include <Enemigo.h>
#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;
using namespace std;

class EnemigoSaltador : public Enemigo
{
    public:
        EnemigoSaltador(float ySuelo = 0.f);
        virtual ~EnemigoSaltador();
        virtual bool update();
        virtual void inicializarPosYDirInicio(float ySuelo = 0.f);

    protected:

    private:
        const float VELOCIDAD_SALTO_INICIAL = 10.f;
        const float VELOCIDAD_SALTO_FINAL = -10.f;
        const float ACELERACION = -1.f;
        float velocidadSaltoActual = 0.f;
};

#endif // ENEMIGOSALTADOR_H
