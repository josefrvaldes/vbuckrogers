#ifndef ELEMENTOPINTABLE_H
#define ELEMENTOPINTABLE_H

#include <SFML/Graphics.hpp>

using namespace sf;

class ElementoPintable
{
    public:
        ElementoPintable();
        virtual ~ElementoPintable();
        virtual void render() {};
        virtual Sprite *getSprite() { return nullptr; };

    protected:
        //Sprite *sprite;

    private:
};

#endif // ELEMENTOPINTABLE_H
