#ifndef ELEMENTOSUELO_H
#define ELEMENTOSUELO_H

#include "SFML/Graphics.hpp"
#include "ElementoPintable.h"
#include <iostream>

using namespace sf;
using namespace std;

class ElementoSuelo : public ElementoPintable
{
    public:
        ElementoSuelo(float inicioSueloY);
        virtual ~ElementoSuelo();
        void setPosActual(Vector2f *vect) { posActual = vect; }
        void setPosSiguiente(Vector2f *vect) { posSiguiente = vect; }
        Vector2f* getPosActual() { return posActual; }
        Vector2f* getPosSiguiente() { return posSiguiente; }

        virtual bool update(bool movimientoLateral = false, bool reinicio = true, float offsetUpdate = 0.f);
        virtual void render();


    protected:
        float inicioSueloY;
        Vector2f *posActual;
        Vector2f *posSiguiente;
        Vector2f *getPosFinInterpolada();
        float getYInterpolada();

        int VEL_MIN = 2;
        int VEL_MAX = 5;

        int POS_X_INI = 0;
        int POS_X_FIN = 0;
        int POS_Y_INI = 180;
        int POS_Y_FIN = 600;


    private:
};

#endif // ELEMENTOSUELO_H
