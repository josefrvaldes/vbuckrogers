#ifndef NIVEL2_H
#define NIVEL2_H

#include <NivelConSuelo.h>

class Nivel2 : public NivelConSuelo
{
    public:
        Nivel2();
        virtual ~Nivel2();
        virtual void update();
        virtual void render();

    protected:
        virtual void crearEnemigo();

    private:
};

#endif // NIVEL2_H
