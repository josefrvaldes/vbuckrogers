#ifndef MAPA_H
#define MAPA_H

#include "SFML/Graphics.hpp"
#include "Constantes.h"
#include "Torreta.h"
#include "NivelConSuelo.h"
#include "RectanguloSuelo.h"
#include <iostream>

using namespace sf;
using namespace std;


class Nivel1 : public NivelConSuelo
{
    public:
        Nivel1();
        virtual ~Nivel1();
        virtual void update();
        virtual void render();

    protected:

    private:
};

#endif // MAPA_H
