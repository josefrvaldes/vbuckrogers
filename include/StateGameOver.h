#ifndef STATEGAMEOVER_H
#define STATEGAMEOVER_H

#include <State.h>
#include "SFML/Graphics.hpp"

using namespace sf;

class StateGameOver : public State
{
    public:
        StateGameOver();
        virtual ~StateGameOver();
        virtual void render();

    protected:

    private:
        Text *textoGameOver;
};

#endif // STATEGAMEOVER_H
