#ifndef SPRITEPARALLAX_H
#define SPRITEPARALLAX_H

#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;
using namespace std;

class SpriteParallax
{
    public:
        SpriteParallax(string file, float yInicio, float velocidad);
        virtual ~SpriteParallax();
        void update();
        void render();

    protected:

    private:
        Sprite *sprite1;
        Sprite *sprite2;
        float velocidad;
};

#endif // SPRITEPARALLAX_H
