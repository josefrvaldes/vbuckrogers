#include <SFML/Graphics.hpp>
#include <iostream>
#include <Juego.h>


using namespace sf;
using namespace std;

int main()
{
    cout << "Hola Mundo!" << endl;

    Juego *juego = Juego::getInstance();
    juego->setState(State::Estados::INGAME);
    juego->iniciar();

    return EXIT_SUCCESS;
}
