#include "Nivel1_2.h"
#include "Nivel2.h"
#include "Jugador.h"
#include "EnemigoBoss.h"
#include "StateInGame.h"

Nivel1_2::Nivel1_2() : NivelEspacio::NivelEspacio()
{
    //ctor
}

Nivel1_2::~Nivel1_2()
{
    //dtor
}

void Nivel1_2::update()
{
    NivelEspacio::update();
    if(avanzarNivel)
    {
        cout << "Nos hemos pasado este nivel!" << endl;
        StateInGame *stateInGame = dynamic_cast<StateInGame*>(Juego::getInstance()->getState());
        stateInGame->setNivel(new Nivel2());
    }
}

void Nivel1_2::render()
{
    NivelEspacio::render();
}

void Nivel1_2::crearEnemigo()
{
    cout << "Estamos llamando a crear enemigo de Nivel1_2" << endl;
    if(Jugador::getInstance()->getNumObjetivos() > 0)
    {
        Enemigo *nuevoEnemigo = new Enemigo(10);
        enemigos->push_back(nuevoEnemigo);
    }
}
