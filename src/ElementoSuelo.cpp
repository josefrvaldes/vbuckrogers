#include "ElementoSuelo.h"
#include "Constantes.h"
#include "Juego.h"
#include "Jugador.h"



ElementoSuelo::ElementoSuelo(float nuevoInicioSueloY)
{
    //ctor
    VEL_MIN = 2;
    VEL_MAX = 5;

    POS_X_INI = 0;
    POS_X_FIN = 0;
    POS_Y_INI = 180;
    POS_Y_FIN = 600;


    inicioSueloY = nuevoInicioSueloY;

    posActual = new Vector2f();
    posSiguiente = new Vector2f();
}

ElementoSuelo::~ElementoSuelo()
{
    //dtor
    delete posActual;
    delete posSiguiente;
}

/**
 *  Devuelve true si se ha salido de la pantalla y false si todavía no.
 */
bool ElementoSuelo::update(bool movimientoLateral, bool reinicio, float offsetUpdate)
{
    if(posSiguiente->y - offsetUpdate <= Constantes::TAMANYO_VENTANA_Y) {
        Vector2f *posAux = new Vector2f();
        *posAux = *posSiguiente;

        getPosSiguiente()->y += Jugador::getVelocidad();
        if(movimientoLateral)
            getPosSiguiente()->x += Jugador::getVelocidad() * Jugador::getInstance()->getVectorDirectorOtros()->x * 2;
        *posActual = *posAux;
        return false;

        // si los rectángulos se han salido de la pantalla, pues los reiniciamos para que vuelvan a estar arriba
    } else if(reinicio) {
        getPosSiguiente()->y = inicioSueloY;
        getPosActual()->y = inicioSueloY;
    }
    return true;
}

void ElementoSuelo::render()
{
    // posición
    Vector2f *posFin = new Vector2f();
    posFin->x = getPosSiguiente()->x; // la x no cambia nunca

    float posFinY = getYInterpolada();
    posFin->y = posFinY;
}

float ElementoSuelo::getYInterpolada()
{
    return (getPosSiguiente()->y - getPosActual()->y) * (float(*Juego::getInstance()->getPercentTick())) + getPosActual()->y;
}

Vector2f *ElementoSuelo::getPosFinInterpolada()
{
    Vector2f *posFin = new Vector2f();
    posFin->x = getPosSiguiente()->x; // la x no cambia nunca

    float posFinY = getYInterpolada();
    posFin->y = posFinY;
    return posFin;
}

