#include "SpriteParallax.h"
#include "Juego.h"
#include "Jugador.h"

#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;
using namespace std;

SpriteParallax::SpriteParallax(string file, float yInicial, float nuevaVelocidad)
{
    //ctor
    Texture *auxText = new Texture();
    auxText->loadFromFile(file);
    sprite1 = new Sprite(*auxText);
    sprite1->setPosition(100, yInicial);
    sprite1->scale(Vector2f(0.5f, 0.3f));

    sprite2 = new Sprite(*auxText);
    sprite2->setPosition(100 + sprite1->getGlobalBounds().width, yInicial);
    sprite2->scale(Vector2f(0.5f, 0.3f));

    velocidad = nuevaVelocidad;
}

SpriteParallax::~SpriteParallax()
{
    //dtor
    delete sprite1;
    delete sprite2;
}

void SpriteParallax::update()
{
    // si se acaban por la izquierda
    if(sprite1->getPosition().x <= -sprite1->getGlobalBounds().width)
    {
        cout << "Cambiamos el parallax de posición" << endl;
        sprite1->setPosition(sprite2->getPosition().x + sprite1->getGlobalBounds().width, sprite1->getPosition().y);
    }
    else if(sprite2->getPosition().x <= -sprite2->getGlobalBounds().width)
    {
        cout << "Cambiamos el parallax de posición" << endl;
        sprite2->setPosition(sprite1->getPosition().x + sprite1->getGlobalBounds().width, sprite2->getPosition().y);
    }
    // si se acaban por la derecha
    else if(sprite1->getPosition().x >= sprite1->getGlobalBounds().width)
    {
        cout << "Cambiamos el parallax de posición" << endl;
        sprite1->setPosition(sprite2->getPosition().x - sprite1->getGlobalBounds().width, sprite1->getPosition().y);
    }
    else if(sprite2->getPosition().x >= sprite1->getGlobalBounds().width)
    {
        cout << "Cambiamos el parallax de posición" << endl;
        sprite2->setPosition(sprite1->getPosition().x - sprite1->getGlobalBounds().width, sprite1->getPosition().y);
    }
    sprite1->setPosition(sprite1->getPosition().x + Jugador::getInstance()->getVectorDirectorOtros()->x * velocidad, sprite1->getPosition().y);
    sprite2->setPosition(sprite2->getPosition().x + Jugador::getInstance()->getVectorDirectorOtros()->x * velocidad, sprite2->getPosition().y);
}

void SpriteParallax::render()
{
    Juego::getInstance()->getWindow()->draw(*sprite1);
    Juego::getInstance()->getWindow()->draw(*sprite2);
}
