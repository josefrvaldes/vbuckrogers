#include "StateJuegoSuperado.h"
#include "Constantes.h"
#include "Juego.h"
#include "State.h"
#include "Motor2D.h"
#include "SFML/Graphics.hpp"

using namespace sf;

StateJuegoSuperado::StateJuegoSuperado()
{
    //ctor
    texto = Motor2D::getDefaultText();
    texto->setString("Tas pasao el juego, fiera!");
    int posX = Constantes::TAMANYO_VENTANA_X / 2 - texto->getGlobalBounds().width / 2;
    int posY = Constantes::TAMANYO_VENTANA_Y / 2 - texto->getGlobalBounds().height / 2;
    texto->setPosition(posX, posY);
}

StateJuegoSuperado::~StateJuegoSuperado()
{
    //dtor
    delete texto;
}

void StateJuegoSuperado::render()
{
    Juego::getInstance()->getWindow()->draw(*texto);
}

