#include "NivelEspacio.h"
#include "Jugador.h"
#include "EnemigoBoss.h"
#include "StateInGame.h"
#include "Constantes.h"

NivelEspacio::NivelEspacio() : Nivel::Nivel()
{
    //ctor
    tiempoEntreEnemigos = 1;
    if(Constantes::DEBUG)
        Jugador::getInstance()->setNumObjetivos(1);
    else
        Jugador::getInstance()->setNumObjetivos(15);
    sumarObjetivoEnemigo = true;
    sumarObjetivoTorreta = false;

    texturaFondo = new Texture();
    texturaFondo->loadFromFile("resources/textures/espacio1.jpg");
    spriteFondo = new Sprite(*texturaFondo);
    Jugador::getInstance()->setPintarSombra(false);
}

NivelEspacio::~NivelEspacio()
{
    //dtor
    delete texturaFondo;
    delete spriteFondo;
}


void NivelEspacio::update()
{
    Nivel::update();
    if(Jugador::getInstance()->getNumObjetivos() <= 0)
    {
        if(bossLlamado == false)
        {
            EnemigoBoss *nuevoEnemigo = new EnemigoBoss();
            enemigos->push_back(nuevoEnemigo);
            bossLlamado = true;
        }
        else if(enemigos->size() <= 0)
        {
            avanzarNivel = true;
            Jugador::getInstance()->setPintarSombra(true);
        }
    }
}

void NivelEspacio::render()
{
    Juego::getInstance()->getWindow()->draw(*spriteFondo);
    Nivel::render();
}

void NivelEspacio::crearEnemigo()
{

}

