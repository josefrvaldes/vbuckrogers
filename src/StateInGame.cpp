#include "StateInGame.h"
#include "Nivel1.h"

StateInGame::StateInGame() : State::State()
{
    //ctor
    nivel = new Nivel1();
    hud = new HUD(nivel);
}

StateInGame::~StateInGame()
{
    //dtor
    delete nivel;
    delete hud;
}

void StateInGame::render()
{
    nivel->render();
    Jugador::getInstance()->render();
    hud->render();
}

void StateInGame::update()
{
    nivel->update();
    hud->update();
    Jugador::getInstance()->update();
}

void StateInGame::setNivel(Nivel *nuevoNivel)
{
    delete nivel;
    nivel = nuevoNivel;
    hud->setNivel(nivel);
}
