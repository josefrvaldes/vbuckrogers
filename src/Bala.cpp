#include "Bala.h"
#include "SFML/Graphics.hpp"
#include "Juego.h"

using namespace sf;

Bala::Bala(Vector2f posicionInicial, Vector2f *nuevoVectorDirector)
{
    //ctor
    vectorDirector = new Vector2f();
    *vectorDirector = *nuevoVectorDirector;
    Texture *texturaBala = new Texture();
    texturaBala->loadFromFile("resources/textures/bala.png");
    spriteBala = new Sprite(*texturaBala);
    spriteBala->setPosition(posicionInicial);
    spriteBala->setScale(0.05f, 0.05f);

    posActual = new Vector2f();
    *posActual = posicionInicial;
    posSiguiente = new Vector2f();
    *posSiguiente = posicionInicial;
}

Bala::~Bala()
{
    //dtor
    delete spriteBala;
    delete vectorDirector;
}

void Bala::update()
{
    Vector2f *aux = new Vector2f();
    *aux = *posSiguiente;
    posSiguiente->x += VELOCIDAD_BALA * vectorDirector->x;
    posSiguiente->y += VELOCIDAD_BALA * vectorDirector->y;
    *posActual = *aux;
}

void Bala::render()
{
    float nuevaX = (posSiguiente->x - posActual->x) * (float(*Juego::getInstance()->getPercentTick())) + posActual->x;
    float nuevaY = (posSiguiente->y - posActual->y) * (float(*Juego::getInstance()->getPercentTick())) + posActual->y;
    spriteBala->setPosition(nuevaX, nuevaY);
    Juego::getInstance()->getWindow()->draw(*spriteBala);
}
