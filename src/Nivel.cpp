#include "Nivel.h"
#include "Juego.h"
#include "Jugador.h"
#include "SFML/Graphics.hpp"

using namespace sf;

Nivel::Nivel()
{
    //ctor
    reloj = new Clock();
    relojEnemigos = new Clock();
    tiempo = new Time();

    tiempoEntreEnemigos = 3;

    tiempoMaxNivel = 120; // en segundos

    enemigos = new vector<Enemigo *>;
    vidas = new vector<Vida *>;
}

Nivel::~Nivel()
{
    //dtor
    delete reloj;
    delete tiempo;
    delete relojEnemigos;
    enemigos->clear();
    enemigos->shrink_to_fit();
    delete enemigos;
}

void Nivel::update()
{
    *tiempo = reloj->getElapsedTime();
    float porcentajeTranscurrido = getPorcentajeTiempoTranscurrido();
    if(porcentajeTranscurrido >= 100)
    {
        if(Juego::getInstance()->getModoDios())
            reloj->restart();
        else
            Juego::getInstance()->setState(State::Estados::GAMEOVER);
    }

    int segundosTranscurridosEnemigos = relojEnemigos->getElapsedTime().asSeconds();
    //cout << "Han transcurrido " << segundosTranscurridosEnemigos << " segundos" << endl;
    if(segundosTranscurridosEnemigos > tiempoEntreEnemigos)
    {
        relojEnemigos->restart();
        //cout << "Hemos reiniciado el reloj" << endl;
        crearEnemigo();
    }

    for(unsigned int i = 0; i < enemigos->size(); i++)
        (*enemigos)[i]->update();


    Jugador::getInstance()->comprobarBalasContraEnemigos(enemigos, sumarObjetivoEnemigo, vidas);

    //cout << "Hay " << vidas->size() << " vidas" << endl;

    for(unsigned int i = 0; i < vidas->size(); i++)
    {
        Vida *v = (*vidas)[i];
        bool saleDePantalla = v->update();
        if(saleDePantalla)
        {
            cout << "El enemigo se salió de la pantalla wey" << endl;
            vidas->erase(vidas->begin()+i);
            delete v;
        }
        else if(v->getSprite()->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds()))
        {
            Jugador::getInstance()->ganarVida();
            vidas->erase(vidas->begin()+i);
            delete v;
        }
    }

    for (unsigned int i = 0; i < enemigos->size(); i++)
    {
        Enemigo *enemigoActual = (*enemigos)[i];
        bool saleDePantalla = enemigoActual->update();
        if(saleDePantalla)
        {
            cout << "El enemigo se salió de la pantalla wey" << endl;
            enemigos->erase(enemigos->begin()+i);
            delete enemigoActual;
        }
        else
        {
            Sprite *auxSprite = new Sprite();
            *auxSprite = *enemigoActual->getSprite();
            Vector2f auxScale = auxSprite->getScale();
            auxScale *= 0.75f;
            auxSprite->setScale(auxScale);
            if(Jugador::getInstance()->getSprite()->getGlobalBounds().intersects(auxSprite->getGlobalBounds()))
            {
                enemigos->erase(enemigos->begin()+i);
                delete enemigoActual;
                Jugador::getInstance()->perderVida();
            }
            delete auxSprite;
        }
    }
}

void Nivel::crearEnemigo()
{
    cout << "Estamos creando un nuevo enemigo" << endl;
}

void Nivel::render()
{
    for(unsigned int i = 0; i < enemigos->size(); i++)
        (*enemigos)[i]->render();

    for(unsigned int i = 0; i < vidas->size(); i++)
        (*vidas)[i]->render();
}
