#include "RectanguloSuelo.h"
#include <iostream>
#include "Constantes.h"
#include "Jugador.h"
#include "Juego.h"

#include "SFML/Graphics.hpp"

using namespace sf;
using namespace std;

RectanguloSuelo::RectanguloSuelo(float nuevoInicioSueloY, int index, int numRectangulosEnPantalla, Color *color) : ElementoSuelo::ElementoSuelo(nuevoInicioSueloY)
{
    //ctor
    rectangulo = new RectangleShape();
    rectangulo->setFillColor(*color);

    Vector2f *pos = new Vector2f(0, nuevoInicioSueloY + (index * (Constantes::TAMANYO_VENTANA_Y - nuevoInicioSueloY) / 4));
    *posActual = *pos;
    *posSiguiente = *pos;

    rectangulo->setPosition(*pos);

    Vector2f *tam = new Vector2f(Constantes::TAMANYO_VENTANA_X, TAMANYO_MIN);
    rectangulo->setSize(*tam);

    TAMANYO_MIN = 0;
    TAMANYO_MAX = ((Constantes::TAMANYO_VENTANA_Y - inicioSueloY) / numRectangulosEnPantalla) - 60;
}

RectanguloSuelo::~RectanguloSuelo()
{
    //dtor
    delete rectangulo;
}



void RectanguloSuelo::update()
{
    ElementoSuelo::update();
}

void RectanguloSuelo::render()
{
    Vector2f *posFin = getPosFinInterpolada();
    getRectangulo()->setPosition(*posFin);
    float tamanyoActual = getTamanyoInterpolado(posFin->y);
    rectangulo->setSize(Vector2f(Constantes::TAMANYO_VENTANA_X, tamanyoActual));
    Juego::getInstance()->getWindow()->draw(*getRectangulo());
}


float RectanguloSuelo::getTamanyoInterpolado(float nuevaY)
{
    return ((nuevaY - (float)POS_Y_INI) / ((float)POS_Y_FIN - (float)POS_Y_INI)) * ((float)TAMANYO_MAX - (float)TAMANYO_MIN) + (float)TAMANYO_MIN;
}
