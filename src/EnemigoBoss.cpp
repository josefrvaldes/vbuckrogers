#include "EnemigoBoss.h"
#include "Funciones.h"
#include "Constantes.h"
#include "Jugador.h"
#include <SFML/Graphics.hpp>

using namespace sf;

EnemigoBoss::EnemigoBoss(float ySuelo) : Enemigo::Enemigo()
{
    //ctor
    velocidad = 1.5f;
    ESCALA_MIN = 1.25f;
    ESCALA_MAX = 2.f;
    sprite->setScale(ESCALA_MIN, ESCALA_MIN);
    inicializarPosYDirInicio();
    relojDisparos = new Clock();
    balas = new vector<Bala *>;
    vida = 5;
    puntuacion = 5000;
}

EnemigoBoss::~EnemigoBoss()
{
    //dtor
    delete relojDisparos;
    balas->clear();
    balas->shrink_to_fit();
    delete balas;
}

bool EnemigoBoss::update()
{
    // el enemigo se está posicionando
    if(!fase2Iniciada && posSiguiente->y < (Constantes::TAMANYO_VENTANA_Y / 2 - sprite->getGlobalBounds().height))
        Enemigo::update();
    else
    {
        if(!fase2Iniciada)
        {
            fase2Iniciada = true;
            vectorDirector->x = 1;
            vectorDirector->y = -1;
        }
        Vector2f *aux = new Vector2f();
        *aux = *posSiguiente;

        posSiguiente->y = posSiguiente->y + vectorDirector->y * velocidad;
        posSiguiente->x = posSiguiente->x + vectorDirector->x * velocidad;

        if(posSiguiente->y <= 0 || posSiguiente->y >= Constantes::TAMANYO_VENTANA_Y / 2 - sprite->getGlobalBounds().height)
            vectorDirector->y *= -1;
        else if(posSiguiente->x <= 0 || posSiguiente->x >= Constantes::TAMANYO_VENTANA_X - sprite->getGlobalBounds().width)
            vectorDirector->x *= -1;

        posSiguiente->x = posSiguiente->x + Jugador::getInstance()->getVectorDirectorOtros()->x  * Jugador::getInstance()->getVelocidad();

        *posActual = *aux;
        disparar();
    }

    for(unsigned int i = 0; i < balas->size(); i++)
    {
        Bala *balaActual = (*balas)[i];
        balaActual->update();
        if(balaActual->getSprite()->getPosition().y > Constantes::TAMANYO_VENTANA_Y)
        {
            balas->erase(balas->begin()+i);
            delete balaActual;
        }
        else if(balaActual->getSprite()->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds()))
        {
            Jugador::getInstance()->perderVida();
            balas->erase(balas->begin()+i);
            delete balaActual;
        }
    }


    return false;
}

void EnemigoBoss::render()
{
    Enemigo::render();
    for(unsigned int i = 0; i < balas->size(); i++)
        (*balas)[i]->render();
}

void EnemigoBoss::inicializarPosYDirInicio(float ySuelo)
{
    cout << "Estamos llamando a inicializarPosYDirInicio del hijo" << endl;
    xOrigen = Funciones::random(0, Constantes::TAMANYO_VENTANA_X);
    yOrigen = 0 - sprite->getGlobalBounds().height;
    vectorDirector = new Vector2f(0, 1);
    posActual = new Vector2f(xOrigen, yOrigen);
    posSiguiente = new Vector2f();
    *posSiguiente = *posActual;
    xFin = xOrigen;
    yFin = Constantes::TAMANYO_VENTANA_Y / 2;
    cout << "La posición de origen es " << xOrigen << "," << yOrigen << " y la de fin es" << xFin << "," << yFin << endl;
}

void EnemigoBoss::disparar()
{
    if(relojDisparos->getElapsedTime().asMilliseconds() > milisEntreDisparos)
    {
        cout << "Estoy disparando, madafaka" << endl;
        relojDisparos->restart();
        Vector2f *positionBala = new Vector2f();
        positionBala->x = sprite->getPosition().x + sprite->getGlobalBounds().width / 2;
        positionBala->y = sprite->getPosition().y + sprite->getGlobalBounds().height / 2;
        Bala *bala = new Bala(*positionBala, new Vector2f(0, 1));
        balas->push_back(bala);
    }
}
