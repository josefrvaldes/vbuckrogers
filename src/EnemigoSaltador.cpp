#include "EnemigoSaltador.h"
#include <Constantes.h>

EnemigoSaltador::EnemigoSaltador(float ySuelo) : Enemigo::Enemigo(ySuelo)
{
    //ctor
    puntuacion = 200;
}

EnemigoSaltador::~EnemigoSaltador()
{
    //dtor
}

bool EnemigoSaltador::update()
{
    Enemigo::update();

    Vector2f *aux = new Vector2f();
    *aux = *posSiguiente;

    if(velocidadSaltoActual < VELOCIDAD_SALTO_FINAL)
        velocidadSaltoActual = VELOCIDAD_SALTO_INICIAL;
    //cout << "La velocidad de salto actual es " << velocidadSaltoActual << endl;
    //cout << "La pos y actual es " << posSiguiente->y << endl;
    posSiguiente->y = posSiguiente->y - velocidadSaltoActual;
    velocidadSaltoActual += ACELERACION;

    *posActual = *aux;

    if(posSiguiente->y > Constantes::TAMANYO_VENTANA_Y + 30 || posSiguiente-> y < 0)
        return true;
    return false;
}

void EnemigoSaltador::inicializarPosYDirInicio(float ySuelo)
{
    Enemigo::inicializarPosYDirInicio(ySuelo);
}
