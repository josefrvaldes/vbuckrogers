#include "Enemigo.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <Funciones.h>
#include <Juego.h>
#include <Jugador.h>
#include <Constantes.h>
#include <math.h>
#include <Motor2D.h>

using namespace sf;
using namespace std;

Enemigo::Enemigo(float ySuelo)
{
    //ctor
    rectangulosDisponibles = new vector<IntRect *>;
    rectangulosDisponibles->push_back(new IntRect(5, 5, 116, 64));
    rectangulosDisponibles->push_back(new IntRect(121, 0, 124, 74));
    rectangulosDisponibles->push_back(new IntRect(254, 0, 120, 74));

    rectangulosDisponibles->push_back(new IntRect(11, 112, 103, 72));
    rectangulosDisponibles->push_back(new IntRect(131, 108, 104, 79));
    rectangulosDisponibles->push_back(new IntRect(258, 112, 112, 72));

    rectangulosDisponibles->push_back(new IntRect(0, 211, 125, 65));
    rectangulosDisponibles->push_back(new IntRect(125, 212, 117, 64));
    rectangulosDisponibles->push_back(new IntRect(257, 210, 115, 68));

    textura = new Texture();
    if(Constantes::DEBUG)
        textura->loadFromFile("resources/textures/ovni_debug.png");
    else
        textura->loadFromFile("resources/textures/ovni.png");
    sprite = new Sprite(*textura);

    int numSprite = Funciones::random(0, rectangulosDisponibles->size() - 1);
    sprite->setTextureRect(*(*rectangulosDisponibles)[numSprite]);
    velocidad = 3.f;

    inicializarPosYDirInicio(ySuelo);
}

Enemigo::~Enemigo()
{
    //dtor
    delete textura;
    delete sprite;
    delete posActual;
    delete posSiguiente;
    delete vectorDirector;
}

bool Enemigo::update()
{
    Vector2f *aux = new Vector2f();
    *aux = *posSiguiente;
    posSiguiente->x = posSiguiente->x + vectorDirector->x * velocidad + Jugador::getInstance()->getVectorDirectorOtros()->x * 2 * Jugador::getInstance()->getVelocidad();
    posSiguiente->y = posSiguiente->y + vectorDirector->y * velocidad;
    *posActual = *aux;

    if(posSiguiente->y > Constantes::TAMANYO_VENTANA_Y + 30 || posSiguiente->y < -100)
        return true;
    return false;
}

void Enemigo::render()
{
    float posFinX = (posSiguiente->x - posActual->x) * *Juego::getInstance()->getPercentTick() + posActual->x;
    float posFinY = (posSiguiente->y - posActual->y) * *Juego::getInstance()->getPercentTick() + posActual->y;

    float escalaActual = 1.f;
    if(yOrigen > yFin)
        escalaActual = Motor2D::getValorInterpolado(posFinY, yOrigen, yFin, ESCALA_MAX, ESCALA_MIN);
    else
        escalaActual = Motor2D::getValorInterpolado(posFinY, yFin, yOrigen, ESCALA_MAX, ESCALA_MIN);
    sprite->setScale(escalaActual, escalaActual);

    sprite->setPosition(posFinX, posFinY);
    Juego::getInstance()->getWindow()->draw(*sprite);
}

void Enemigo::inicializarPosYDirInicio(float ySuelo)
{
    cout << "Estamos llamando a inicializarPosYDirInicio del padre" << endl;
    bool arriba = Funciones::random(0, 1) == 1;

    if(arriba)
    {
        yOrigen = ySuelo - sprite->getGlobalBounds().height;
        yFin = Constantes::TAMANYO_VENTANA_Y;
    }
    else
    {
        yOrigen = Constantes::TAMANYO_VENTANA_Y;
        yFin = ySuelo - sprite->getGlobalBounds().height;
    }

    xOrigen = Funciones::random(0, Constantes::TAMANYO_VENTANA_X);
    // significa que irá de izquierda a derecha
    if(xOrigen < Constantes::TAMANYO_VENTANA_X / 2)
        xFin = Funciones::random(Constantes::TAMANYO_VENTANA_X / 2, Constantes::TAMANYO_VENTANA_X);

        // irá de derecha a izquierda
    else
        xFin = Funciones::random(0, Constantes::TAMANYO_VENTANA_X / 2);

    float xAux  = xFin - xOrigen;
    float yAux  = yFin - yOrigen;

    //cout << "La posición de origen es " << xOrigen << "," << yOrigen << " y la de fin es" << xFin << "," << yFin << endl;


    float modulo = sqrt(pow(xAux, 2) + pow(yAux, 2));
    vectorDirector = new Vector2f((1 / modulo) * xAux, (1 / modulo) * yAux);
    //cout << "El vector director es " << vectorDirector->x << "," << vectorDirector->y << endl;

    posActual = new Vector2f(xOrigen, yOrigen);
    posSiguiente = new Vector2f();
    *posSiguiente = *posActual;
}
