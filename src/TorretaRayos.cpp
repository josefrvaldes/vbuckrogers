#include "TorretaRayos.h"
#include "SFML/Graphics.hpp"
#include "Juego.h"
#include "Jugador.h"
#include <iostream>

using namespace sf;
using namespace std;

TorretaRayos::TorretaRayos(float posX, float nuevoInicioSuelo) : Torreta::Torreta(posX, nuevoInicioSuelo)
{
    //ctor
    texturaRayos = new Texture();
    texturaRayos->loadFromFile("resources/textures/lightning.png");
    relojTextura = new Clock();
    spriteRayosDch = new Sprite(*texturaRayos);
    spriteRayosDch->setTextureRect(IntRect(ANCHO_TEXTURA * textActual, 0, ANCHO_TEXTURA, ALTO_TEXTURA));
    spriteRayosDch->setScale(0.5f, 1.0f);
    spriteRayosDch->setRotation(90.f);

    spriteRayosIzq = new Sprite(*texturaRayos);
    spriteRayosIzq->setTextureRect(IntRect(ANCHO_TEXTURA * textActual, 0, ANCHO_TEXTURA, ALTO_TEXTURA));
    spriteRayosIzq->setScale(-0.5f, 1.f);
    spriteRayosIzq->setRotation(-90.f);
}

TorretaRayos::~TorretaRayos()
{
    //dtor
    delete spriteRayosDch;
    delete relojTextura;
    delete texturaRayos;
}

bool TorretaRayos::update(bool reinicio)
{
    int milisTranscurridos = relojTextura->getElapsedTime().asMilliseconds();
    if(milisTranscurridos > MILISEGUNDOS_TEXTURA)
    {
        relojTextura->restart();
        if(textActual < NUM_TEXTURAS)
            textActual++;
        else
            textActual = 0;
        spriteRayosDch->setTextureRect(IntRect(ANCHO_TEXTURA * textActual, 0, ANCHO_TEXTURA, ALTO_TEXTURA));
        spriteRayosIzq->setTextureRect(IntRect(ANCHO_TEXTURA * textActual, 0, ANCHO_TEXTURA, ALTO_TEXTURA));
    }
    return Torreta::update(reinicio);
}

void TorretaRayos::render()
{
    Torreta::render();
    // por algún motivo esto está invertido
    float xIzq = spriteDch->getPosition().x;
    float yIzq = spriteDch->getPosition().y;
    float xDch = spriteIzq->getPosition().x;
    float yDch = spriteIzq->getPosition().y;


    spriteRayosDch->setPosition(xDch + 10, yDch + spriteIzq->getGlobalBounds().height / 2);
    spriteRayosIzq->setPosition(xIzq - 10 + spriteIzq->getGlobalBounds().width, yIzq + spriteIzq->getGlobalBounds().height / 2);
    Juego::getInstance()->getWindow()->draw(*spriteRayosDch);
    Juego::getInstance()->getWindow()->draw(*spriteRayosIzq);
}

bool TorretaRayos::chocaConJugador()
{
    bool haChocado = Torreta::chocaConJugador();
    if(haChocado)
        return true;
    int margen = 30;
    float yBaseTorreta = spriteDch->getPosition().y + spriteDch->getGlobalBounds().height;
    float ySombra = Jugador::getInstance()->getPosYSombra();
    if(yBaseTorreta >= ySombra - margen && yBaseTorreta <= ySombra + margen &&
        (spriteRayosDch->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds())
        || spriteRayosIzq->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds())))
    {
        return true;
    }
    return false;
}
