#include "Nivel2_2.h"
#include "Nivel3.h"
#include "Jugador.h"
#include "EnemigoBoss.h"
#include "StateInGame.h"


Nivel2_2::Nivel2_2() : NivelEspacio::NivelEspacio()
{
    //ctor
    texturaFondo->loadFromFile("resources/textures/espacio2.jpg");
}

Nivel2_2::~Nivel2_2()
{
    //dtor
}


void Nivel2_2::update()
{
    NivelEspacio::update();
    if(avanzarNivel)
    {
        cout << "Nos hemos pasado este nivel!" << endl;
        StateInGame *stateInGame = dynamic_cast<StateInGame*>(Juego::getInstance()->getState());
        stateInGame->setNivel(new Nivel3(true));
    }
}

void Nivel2_2::render()
{
    NivelEspacio::render();
}

void Nivel2_2::crearEnemigo()
{
    cout << "Estamos llamando a crear enemigo de Nivel1_2" << endl;
    if(Jugador::getInstance()->getNumObjetivos() > 0)
    {
        Enemigo *nuevoEnemigo = new Enemigo(10);
        enemigos->push_back(nuevoEnemigo);
    }
}
