#include "NivelConSuelo.h"
#include "Funciones.h"
#include "SpriteParallax.h"
#include "EnemigoSaltador.h"
#include "EnemigoBoss.h"
#include "Jugador.h"
#include "TorretaRayos.h"
#include "Juego.h"
#include "SFML/Graphics.hpp"

using namespace sf;

NivelConSuelo::NivelConSuelo(bool newTorretasConRayos)
{
    //ctor
    if(Constantes::DEBUG)
        Jugador::getInstance()->setNumObjetivos(1);
    else
        Jugador::getInstance()->setNumObjetivos(15);

    torretasConRayos = newTorretasConRayos;
    colorRectangulo = new Color(rSuelo, gSuelo, bSuelo);
    rectangulosSuelo = new vector<RectanguloSuelo *>;
    inicializarRectangulosSuelo();

    torretas = new vector<Torreta *>;
    inicializarTorretas();

    Texture *auxText = new Texture();
    auxText->loadFromFile("resources/textures/suelo_marron.png");
    spriteSuelo = new Sprite(*auxText);
    spriteSuelo->setPosition(POS_INICIAL_SUELO_X, POS_INICIAL_SUELO_Y);


    spriteFondoBack = new SpriteParallax("resources/textures/mountains-back.png", 0.f, velocidadParallaxBack);
    spriteFondoMid = new SpriteParallax("resources/textures/mountains-mid1.png", 124 * 0.3f, velocidadParallaxMid);
    spriteFondoFront = new SpriteParallax("resources/textures/mountains-mid2.png", 412 * 0.3f, velocidadParallaxFront);
    sumarObjetivoEnemigo = false;
    sumarObjetivoTorreta = true;
}

NivelConSuelo::~NivelConSuelo()
{
    //dtor
    delete spriteFondoBack;
    delete spriteFondoFront;
    delete spriteFondoMid;
    delete spriteSuelo;
    rectangulosSuelo->clear();
    rectangulosSuelo->shrink_to_fit();
    delete rectangulosSuelo;
    torretas->clear();
    torretas->shrink_to_fit();
    delete torretas;
}

void NivelConSuelo::inicializarRectangulosSuelo()
{
    int numRectangulosEnPantalla = 4;
    for(int i = 0; i < numRectangulosEnPantalla; i++)
    {
        RectanguloSuelo *rectangulo = new RectanguloSuelo(POS_INICIAL_SUELO_Y, i, numRectangulosEnPantalla, colorRectangulo);
        rectangulosSuelo->push_back(rectangulo);
    }
}

void NivelConSuelo::inicializarTorretas()
{
    Torreta *torreta ;
    if(torretasConRayos)
        torreta = new TorretaRayos(Constantes::TAMANYO_VENTANA_X / 2, POS_INICIAL_SUELO_Y);
    else
        torreta = new Torreta(POS_INICIAL_SUELO_Y);
    torretas->push_back(torreta);
}

void NivelConSuelo::update()
{
    Nivel::update();
    for(unsigned int i = 0; i < rectangulosSuelo->size(); i++)
    {
        RectanguloSuelo *rectActual = (*rectangulosSuelo)[i];
        rectActual->update();
    }


    Vector2f *vectorOtros = new Vector2f();
    *vectorOtros = *Jugador::getInstance()->getVectorDirectorOtros();


    spriteFondoBack->update();
    spriteFondoMid->update();
    spriteFondoFront->update();


    for(unsigned int i = 0; i < torretas->size(); i++)
    {
        Torreta *torretaActual = (*torretas)[i];
        bool saleDePantalla = torretaActual->update(false);
        if(saleDePantalla)
        {
            cout << "La torreta se salió de la pantalla wey" << endl;
            torretas->erase(torretas->begin()+i);
            delete torretaActual;

            int nuevaX = Funciones::random(0, Constantes::TAMANYO_VENTANA_X);
            Torreta *nuevaTorreta;
            if(torretasConRayos)
                nuevaTorreta = new TorretaRayos(nuevaX, POS_INICIAL_SUELO_Y);
            else
                nuevaTorreta = new Torreta(nuevaX, POS_INICIAL_SUELO_Y);
            torretas->push_back(nuevaTorreta);
        }
        else
        {
            if(!torretaActual->getTorretaChocada() && torretaActual->chocaConJugador())
            {
                cout << "Intersecta madafaka!!" << endl;
                torretaActual->chocar();
                Jugador::getInstance()->perderVida();
            }
            else if(!torretaActual->getTorretaConeguida() && torretaActual->seConsigueTorreta())
            {
                cout << "Torreta conseguida" << endl;
                torretaActual->conseguir();
                Jugador::getInstance()->objetivoConseguido();
                if(Jugador::getInstance()->getNumObjetivos() <= 0)
                {
                    cout << "Pantalla superada" << endl;
                }
            }
        }
    }
}

void NivelConSuelo::render()
{
    spriteFondoBack->render();
    spriteFondoMid->render();
    spriteFondoFront->render();
    Juego::getInstance()->getWindow()->draw(*spriteSuelo);

    for(unsigned int i = 0; i < rectangulosSuelo->size(); i++)
        (*rectangulosSuelo)[i]->render();

    vector<ElementoPintable *> *spritesAPintar = new vector<ElementoPintable *>;
    for(unsigned int i = 0; i < torretas->size(); i++)
        spritesAPintar->push_back((*torretas)[i]);

    for(unsigned int i = 0; i < enemigos->size(); i++)
        spritesAPintar->push_back((*enemigos)[i]);

    for(unsigned int i = 0; i < vidas->size(); i++)
        spritesAPintar->push_back((*vidas)[i]);

    // ordenamos las torretas y los enemigos para que se pinten en orden coherente
    // (que si una torreta está en primer plano no se le pinte encima un enemigo que está al fondo)
    sort(spritesAPintar->begin(), spritesAPintar->end(), [](ElementoPintable *lhs, ElementoPintable *rhs)
    {
        return lhs->getSprite()->getPosition().y < rhs->getSprite()->getPosition().y;
    });

    for(unsigned int i = 0; i < spritesAPintar->size(); i++)
        (*spritesAPintar)[i]->render();
}

void NivelConSuelo::crearEnemigo()
{
    Enemigo *nuevoEnemigo = new Enemigo(POS_INICIAL_SUELO_Y);
    enemigos->push_back(nuevoEnemigo);
}
