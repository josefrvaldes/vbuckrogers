#include "Nivel3.h"
#include "Jugador.h"
#include "StateInGame.h"
#include "Funciones.h"
#include "EnemigoSaltador.h"
#include "Constantes.h"
#include "TorretaRayos.h"

Nivel3::Nivel3(bool newTorretasConRayos) : NivelConSuelo::NivelConSuelo(newTorretasConRayos)
{
    //ctor
    rSuelo = 171;
    gSuelo = 42;
    bSuelo = 62;
    cout << "Estamos en constructor Nivel3" << endl;
    if(Constantes::DEBUG)
        Jugador::getInstance()->setNumObjetivos(1);
    else
        Jugador::getInstance()->setNumObjetivos(20);

    for(unsigned int i = 0; i < rectangulosSuelo->size(); i++)
        (*rectangulosSuelo)[i]->getRectangulo()->setFillColor(Color(rSuelo, gSuelo, bSuelo));
    torretasConRayos = true;
}

Nivel3::~Nivel3()
{
    //dtor
}

void Nivel3::update()
{
    NivelConSuelo::update();
    if(Jugador::getInstance()->getNumObjetivos() <= 0)
    {
        Juego::getInstance()->setState(State::SUPERADO);
    }
}


void Nivel3::render()
{
    NivelConSuelo::render();
}

void Nivel3::crearEnemigo()
{
    int tipoEnemigo = Funciones::random(0, 1);
    Enemigo *nuevoEnemigo;
    if(tipoEnemigo == 0)
        nuevoEnemigo = new Enemigo(POS_INICIAL_SUELO_Y);
    else
        nuevoEnemigo = new EnemigoSaltador(POS_INICIAL_SUELO_Y);
    enemigos->push_back(nuevoEnemigo);
}


