#include "Vida.h"
#include "SFML/Graphics.hpp"
#include "Motor2D.h"
#include "Juego.h"
#include <iostream>

using namespace sf;
using namespace std;

Vida::Vida(float inicioSueloY, float x, float y) : ElementoSuelo(inicioSueloY)
{
    //ctor
    textura = new Texture();
    textura->loadFromFile("resources/textures/vida.png");
    sprite = new Sprite(*textura);
    posActual->x = x;
    posActual->y = y;
    *posSiguiente = *posActual;
    sprite->setPosition(*posActual);
    sprite->setScale(ESCALA_INICIAL, ESCALA_INICIAL);
}

Vida::~Vida()
{
    //dtor
    delete textura;
    delete sprite;
}


void Vida::render()
{
    Vector2f *posFin = getPosFinInterpolada();
    posFin->y -= sprite->getGlobalBounds().height;

    sprite->setPosition(posFin->x, posFin->y);

    float escala = Motor2D::getValorInterpolado(posFin->y, POS_Y_INI, POS_Y_FIN, ESCALA_INICIAL, ESCALA_FINAL);
    sprite->setScale(escala, escala);

    Juego::getInstance()->getWindow()->draw(*sprite);
}

bool Vida::update()
{
    return ElementoSuelo::update(true, false, 0.f);
}
