#include "SFML/Graphics.hpp"
#include "Constantes.h"
#include "Torreta.h"
#include "Juego.h"
#include "Jugador.h"
#include "Motor2D.h"
#include <iostream>

using namespace sf;
using namespace std;

Torreta::Torreta(float posX, float nuevoInicioSuelo) : ElementoSuelo::ElementoSuelo(nuevoInicioSuelo)
{
    //ctor
    Texture *textura = new Texture();
    textura->loadFromFile("resources/textures/torreta.png");
    spriteIzq = new Sprite(*textura);
    spriteDch = new Sprite(*textura);
    spriteIzq->scale(ESCALA_INICIAL_X, ESCALA_INICIAL_Y);
    spriteDch->scale(ESCALA_INICIAL_X, ESCALA_INICIAL_Y);

    //float altoSprite = spriteIzq->getGlobalBounds().height;
    posActual->x = posX;
    posActual->y = nuevoInicioSuelo;
    posSiguiente->x = posX;
    posSiguiente->y = nuevoInicioSuelo;

    spriteIzq->setPosition(*posActual);
    spriteDch->setPosition(posActual->x + SEPARACION_INICIAL, posActual->y);

    torretaConseguida = false;
    torretaChocada = false;
}

Torreta::Torreta(float nuevoInicioSuelo) : Torreta::Torreta(Constantes::TAMANYO_VENTANA_X / 2, nuevoInicioSuelo)
{

}

Torreta::~Torreta()
{
    //dtor
    cout << "Estamos en destructor de torreta" << endl;
    delete spriteIzq;
    delete spriteDch;
}

bool Torreta::update(bool reinicio)
{
    return ElementoSuelo::update(true, reinicio, spriteIzq->getGlobalBounds().height);
}

void Torreta::render()
{
    Vector2f *posFin = getPosFinInterpolada();
    posFin->y -= spriteDch->getGlobalBounds().height;

    int separacionInterpolada = getSeparacionInterpolada(posFin->y);

    spriteIzq->setPosition(posFin->x - separacionInterpolada / 2, posFin->y);
    spriteDch->setPosition(posFin->x + separacionInterpolada / 2, posFin->y);

    Vector2f *scaleFin = getEscalaInterpolada(posFin->y);
    spriteIzq->setScale(scaleFin->x, scaleFin->y);
    spriteDch->setScale(scaleFin->x, scaleFin->y);

    Juego::getInstance()->getWindow()->draw(*spriteIzq);
    Juego::getInstance()->getWindow()->draw(*spriteDch);
}

Vector2f *Torreta::getEscalaInterpolada(float nuevaY)
{
    float escalaX = Motor2D::getValorInterpolado(nuevaY, POS_Y_INI, POS_Y_FIN, ESCALA_INICIAL_X, ESCALA_FINAL_X);
    float escalaY = Motor2D::getValorInterpolado(nuevaY, POS_Y_INI, POS_Y_FIN, ESCALA_INICIAL_Y, ESCALA_FINAL_Y);
    return new Vector2f(escalaX, escalaY);
}

int Torreta::getSeparacionInterpolada(float nuevaY)
{
    return Motor2D::getValorInterpolado(nuevaY, POS_Y_INI, POS_Y_FIN, SEPARACION_INICIAL, SEPARACION_FINAL);
}

Sprite* Torreta::getSprite()
{
    return spriteIzq;
}

bool Torreta::chocaConJugador()
{
    int margen = 30;
    float yBaseTorreta = spriteDch->getPosition().y + spriteDch->getGlobalBounds().height;
    float ySombra = Jugador::getInstance()->getPosYSombra();
    //float xTorretaIzq = spriteIzq->getPosition().x + spriteIzq->getGlobalBounds().width;
    //float xTorretaDch = spriteDch->getPosition().x;
    //float xJugador = Jugador::getInstance()->getSprite()->getPosition().x;
    //float yJugador = Jugador::getInstance()->getSprite()->getPosition().y;
    //if(spriteDch->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds())
    //    || spriteIzq->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds()))
    Sprite *auxDch = new Sprite();
    *auxDch = *spriteDch;
    Sprite *auxIzq = new Sprite();
    *auxIzq = *spriteIzq;
    Vector2f auxScale = auxDch->getScale();
    auxScale *= 0.75f;
    auxDch->setScale(auxScale);
    auxIzq->setScale(auxScale);
    bool salida = false;
    if(yBaseTorreta >= ySombra - margen && yBaseTorreta <= ySombra + margen &&
        (auxDch->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds())
        || auxIzq->getGlobalBounds().intersects(Jugador::getInstance()->getSprite()->getGlobalBounds())))
    {
        salida = true;
    }
    delete auxDch;
    delete auxIzq;
    return salida;
}

bool Torreta::seConsigueTorreta()
{
    float yBaseTorreta = spriteDch->getPosition().y + spriteDch->getGlobalBounds().height;
    float xTorretaIzq = spriteIzq->getPosition().x + spriteIzq->getGlobalBounds().width;
    float xTorretaDch = spriteDch->getPosition().x;
    float xJugador = Jugador::getInstance()->getSprite()->getPosition().x;
    float yJugador = Jugador::getInstance()->getSprite()->getPosition().y;
    if(xJugador >= xTorretaIzq
                && xJugador <= xTorretaDch
                && yJugador >= yBaseTorreta - 10
                && yJugador <= yBaseTorreta + 10)
        return true;
    return false;
}
