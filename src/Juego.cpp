#include "Juego.h"
#include "State.h"
#include "StateInGame.h"
#include "StateGameOver.h"
#include "StateJuegoSuperado.h"
#include "Constantes.h"
#include "SFML/Graphics.hpp"

using namespace std;
using namespace sf;

Juego* Juego::juego = 0;

Juego::Juego()
{
    //ctor
    nombre = new string("VBuck Rogers");
    direccionView = new Vector2f();
    updateClock = new Clock();
    timeElapsed = new Time();
    percentTick = new double();
    *percentTick = 0.f;
}

Juego::~Juego()
{
    //dtor
    cout << "Estamos destruyendo la clase juego" << endl;
    delete nombre;
    delete direccionView;
    delete updateClock;
    delete timeElapsed;
    delete percentTick;
}

Juego* Juego::getInstance()
{
    if(juego == 0)
    {
        juego = new Juego();
    }
    return juego;
}

void Juego::setState(State::Estados tipoEstado)
{
    switch(tipoEstado) {
        case State::INGAME:
            stateActual = new StateInGame();
            break;
        case State::GAMEOVER:
            stateActual = new StateGameOver();
            break;
        case State::SUPERADO:
            stateActual = new StateJuegoSuperado();
            break;
        default:
            cout << "Este estado no existe." << endl;
    }
}

void Juego::iniciar()
{
    window = new RenderWindow(sf::VideoMode(Constantes::TAMANYO_VENTANA_X, Constantes::TAMANYO_VENTANA_Y), "MSXPlanet");
    view = new View();
    view->setSize(Vector2f(Constantes::TAMANYO_VENTANA_X, Constantes::TAMANYO_VENTANA_Y));
    view->setCenter(Constantes::TAMANYO_VENTANA_X / 2, Constantes::TAMANYO_VENTANA_Y / 2);
    buclePrincipal();
}


void Juego::buclePrincipal()
{
    const double UPDATE_TICK_TIME = 1000.0 / 30.0;

    updateClock->restart();

    while (window->isOpen())
    {
        Event event;
        while (window->pollEvent(event))
        {
            switch(event.type)
            {
                case Event::Closed:
                    window->close();
                    break;

                case Event::KeyPressed:
                    if(event.key.code == Keyboard::Escape)
                        window->close();
                    break;
                case Event::KeyReleased:
                    if(event.key.code == Keyboard::G)
                    {
                        modoDios = !modoDios;
                        cout << "El modo dios está " << modoDios << endl;
                    }
                    break;
            }
        }


        if(updateClock->getElapsedTime().asMilliseconds() > UPDATE_TICK_TIME)
        {
            *timeElapsed = updateClock->restart();  // este valor hay que usarlo para calcular el movimiento de forma correcta timeElapsed.asSeconds()
            stateActual->update();
        }
        *percentTick = min(1.0, updateClock->getElapsedTime().asMilliseconds() / (UPDATE_TICK_TIME * 1.d));
        window->clear();
        window->setView(*view);
        stateActual->render();

        window->display();
    }
}


