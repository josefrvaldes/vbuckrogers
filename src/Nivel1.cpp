#include "Nivel1.h"
#include "Nivel1_2.h"
#include "Jugador.h"
#include "Juego.h"
#include "StateInGame.h"


Nivel1::Nivel1() : NivelConSuelo::NivelConSuelo()
{
    //ctor
}

Nivel1::~Nivel1()
{
    //dtor
}


void Nivel1::update()
{
    NivelConSuelo::update();
    if(Jugador::getInstance()->getNumObjetivos() <= 0)
    {
        StateInGame *stateInGame = dynamic_cast<StateInGame*>(Juego::getInstance()->getState());
        stateInGame->setNivel(new Nivel1_2());
    }
}


void Nivel1::render()
{
    NivelConSuelo::render();
}




