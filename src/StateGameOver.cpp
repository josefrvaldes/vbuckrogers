#include "StateGameOver.h"
#include "Constantes.h"
#include "Juego.h"
#include "State.h"
#include "Motor2D.h"
#include "SFML/Graphics.hpp"

using namespace sf;

StateGameOver::StateGameOver() : State::State()
{
    //ctor
    textoGameOver = Motor2D::getDefaultText();
    textoGameOver->setString("Game over, pringao");
    int posX = Constantes::TAMANYO_VENTANA_X / 2 - textoGameOver->getGlobalBounds().width / 2;
    int posY = Constantes::TAMANYO_VENTANA_Y / 2 - textoGameOver->getGlobalBounds().height / 2;
    textoGameOver->setPosition(posX, posY);
}

StateGameOver::~StateGameOver()
{
    //dtor
    delete textoGameOver;
}

void StateGameOver::render()
{
    Juego::getInstance()->getWindow()->draw(*textoGameOver);
}
