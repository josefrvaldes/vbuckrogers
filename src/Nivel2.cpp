#include "Nivel2.h"
#include "Nivel2_2.h"
#include "Jugador.h"
#include "StateInGame.h"
#include "Funciones.h"
#include "EnemigoSaltador.h"
#include "Constantes.h"

Nivel2::Nivel2() : NivelConSuelo::NivelConSuelo()
{
    //ctor
    rSuelo = 171;
    gSuelo = 42;
    bSuelo = 62;
    cout << "Estamos en constructor Nivel2" << endl;
    if(Constantes::DEBUG)
        Jugador::getInstance()->setNumObjetivos(1);
    else
        Jugador::getInstance()->setNumObjetivos(15);

    for(unsigned int i = 0; i < rectangulosSuelo->size(); i++)
        (*rectangulosSuelo)[i]->getRectangulo()->setFillColor(Color(rSuelo, gSuelo, bSuelo));
}

Nivel2::~Nivel2()
{
    //dtor
}

void Nivel2::update()
{
    NivelConSuelo::update();
    if(Jugador::getInstance()->getNumObjetivos() <= 0)
    {
        StateInGame *stateInGame = dynamic_cast<StateInGame*>(Juego::getInstance()->getState());
        stateInGame->setNivel(new Nivel2_2());
    }
}


void Nivel2::render()
{
    NivelConSuelo::render();
}

void Nivel2::crearEnemigo()
{
    int tipoEnemigo = Funciones::random(0, 1);
    Enemigo *nuevoEnemigo;
    if(tipoEnemigo == 0)
        nuevoEnemigo = new Enemigo(POS_INICIAL_SUELO_Y);
    else
        nuevoEnemigo = new EnemigoSaltador(POS_INICIAL_SUELO_Y);
    enemigos->push_back(nuevoEnemigo);
}
