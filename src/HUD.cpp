#include "HUD.h"

#include "SFML/Graphics.hpp"
#include "Motor2D.h"
#include "Juego.h"
#include "Jugador.h"
#include <iostream>
#include <math.h>

using namespace std;
using namespace sf;

HUD::HUD(Nivel *nuevoNivel)
{
    //ctor
    nivel = nuevoNivel;
    lblTime = Motor2D::getDefaultText();
    lblTime->setString("Time");
    lblTime->setPosition(POS_LBL_TIME_X, POS_LBL_TIME_Y);
    lblScore = Motor2D::getDefaultText();
    lblScore->setString("Score");
    lblScore->setPosition(POS_LBL_SCORE_X, POS_LBL_SCORE_Y);
    txtScore = Motor2D::getDefaultText();
    txtScore->setString(getStringFromPuntuacion(Jugador::getInstance()->getPuntuacion()));
    txtScore->setPosition(POS_TXT_SCORE_X, POS_TXT_SCORE_Y);

    barraVidaBlanca = new RectangleShape();
    barraVidaBlanca->setFillColor(Color::White);
    barraVidaBlanca->setPosition(POS_BARRA_BLANCA_X, POS_BARRA_BLANCA_Y);
    barraVidaBlanca->setSize(Vector2f(WIDTH_BARRA_BLANCA, HEIGHT_BARRA_BLANCA));

    barraVidaVerde = new RectangleShape();
    barraVidaVerde->setFillColor(Color::Green);
    barraVidaVerde->setPosition(POS_BARRA_VERDE_X, POS_BARRA_VERDE_Y);
    barraVidaVerde->setSize(Vector2f(WIDTH_BARRA_VERDE, HEIGHT_BARRA_VERDE));

    textureVida = new Texture();
    textureVida->loadFromFile("resources/textures/spaceship.png");

    textureObjetivo = new Texture();
    textureObjetivo->loadFromFile("resources/textures/objetivo.png");

    Texture *textureModoDios = new Texture();
    textureModoDios->loadFromFile("resources/textures/jesus.png");
    spriteModoDios = new Sprite(*textureModoDios);
    spriteModoDios->setPosition(POS_MODO_DIOS_X, POS_MODO_DIOS_Y);
    spriteModoDios->setScale(ESCALA_MODO_DIOS, ESCALA_MODO_DIOS);



    spritesVidas = new vector<Sprite *>;
    spritesObjetivos = new vector<Sprite *>;
}

HUD::~HUD()
{
    //dtor
    delete lblTime;
    delete lblScore;
    delete barraVidaBlanca;
    delete barraVidaVerde;

    delete spritesVidas;
    delete spritesObjetivos;

    delete textureVida;
    delete textureObjetivo;
    delete spriteModoDios;
}


void HUD::render()
{
    Juego::getInstance()->getWindow()->draw(*lblTime);
    Juego::getInstance()->getWindow()->draw(*lblScore);
    Juego::getInstance()->getWindow()->draw(*txtScore);
    Juego::getInstance()->getWindow()->draw(*barraVidaBlanca);
    Juego::getInstance()->getWindow()->draw(*barraVidaVerde);
    for(unsigned int i = 0; i < spritesVidas->size(); i++)
    {
        Sprite *spriteActual = (*spritesVidas)[i];
        Juego::getInstance()->getWindow()->draw(*spriteActual);
    }
    for(unsigned int i = 0; i < spritesObjetivos->size(); i++)
    {
        Sprite *spriteActual = (*spritesObjetivos)[i];
        Juego::getInstance()->getWindow()->draw(*spriteActual);
    }
    if(Juego::getInstance()->getModoDios())
        Juego::getInstance()->getWindow()->draw(*spriteModoDios);
}



void HUD::update()
{
    txtScore->setString(getStringFromPuntuacion(Jugador::getInstance()->getPuntuacion()));

    float porcentajeTiempo = nivel->getPorcentajeTiempoTranscurrido();
    float nuevoWidth = WIDTH_BARRA_VERDE - porcentajeTiempo / 100 * WIDTH_BARRA_VERDE;
    barraVidaVerde->setSize(Vector2f(nuevoWidth, HEIGHT_BARRA_VERDE));

    if(Jugador::getInstance()->getVidas() != (int)spritesVidas->size())
    {
        spritesVidas->clear();
        spritesVidas->shrink_to_fit();
        for(int i = 0; i < Jugador::getInstance()->getVidas(); i++)
        {
            Sprite *nuevoSprite = new Sprite(*textureVida);
            nuevoSprite->setScale(ESCALA_SPRITE_VIDAS, ESCALA_SPRITE_VIDAS);
            nuevoSprite->setPosition(POS_INICIO_VIDAS_X + i * nuevoSprite->getGlobalBounds().width + MARGEN * 2, POS_INICIO_VIDAS_Y);
            spritesVidas->push_back(nuevoSprite);
        }
    }

    if(Jugador::getInstance()->getNumObjetivos() != (int)spritesObjetivos->size())
    {
        spritesObjetivos->clear();
        spritesObjetivos->shrink_to_fit();
        for(int i = 0; i < Jugador::getInstance()->getNumObjetivos(); i++)
        {
            Sprite *nuevoSprite = new Sprite(*textureObjetivo);
            nuevoSprite->setScale(ESCALA_SPRITE_OBJETIVOS, ESCALA_SPRITE_OBJETIVOS);

            int fila = floor(i / 8);
            int col = floor(i % 8);

            nuevoSprite->setPosition(
                POS_INICIO_OBJETIVOS_X + col * nuevoSprite->getGlobalBounds().width + MARGEN * 2,
                POS_INICIO_OBJETIVOS_Y + fila * nuevoSprite->getGlobalBounds().width + MARGEN * 2
            );
            spritesObjetivos->push_back(nuevoSprite);
        }
    }
}


void HUD::setNivel(Nivel *nuevoNivel)
{
    //if(nivel != nullptr && nivel != NULL)
    //    delete nivel;
    nivel = nuevoNivel;
}
