#include "Jugador.h"
#include "Juego.h"
#include "Funciones.h"
#include "SFML/Graphics.hpp"
#include <iostream>

using namespace sf;
using namespace std;


Jugador* Jugador::jugador = 0;

float Jugador::velocidadZ = 3.f;

Jugador::Jugador()
{
    //ctor
    Texture *textura = new Texture();
    textura->loadFromFile("resources/textures/spaceship.png");
    sprite = new Sprite(*textura);
    sprite->setPosition(POS_INICIAL_X, POS_INICIAL_Y);
    sprite->setOrigin(Vector2f(46.f, 40.f)); // para centrar el eje de giro

    Texture *texturaSombra = new Texture();
    texturaSombra->loadFromFile("resources/textures/spaceship_shadow.png");
    spriteSombra = new Sprite(*texturaSombra);
    spriteSombra->setPosition(POS_INICIAL_X, POS_Y_SOMBRA);
    spriteSombra->setOrigin(Vector2f(46.f, 40.f)); // para centrar el eje de giro

    vectorDirector = new Vector2f();
    vectorDirectorOtros = new Vector2f();
    posActual = new Vector2f(POS_INICIAL_X, POS_INICIAL_Y);
    posSiguiente = new Vector2f(POS_INICIAL_X, POS_INICIAL_Y);
    vidas = 3;
    salud = 80;
    puntuacion = new int(0);
    numObjetivos = 2;

    balas = new vector<Bala *>;
    relojCadencia = new Clock();
}

Jugador *Jugador::getInstance()
{
    if(jugador == 0)
    {
        jugador = new Jugador();
    }
    return jugador;
}

Jugador::~Jugador()
{
    //dtor
    delete sprite;
    delete spriteSombra;
    delete vectorDirector;
    delete posActual;
    delete posSiguiente;
    delete puntuacion;
    balas->clear();
    balas->shrink_to_fit();
    delete balas;

    delete relojCadencia;
}

void Jugador::render() {
    // posición
    float posFinX = (posSiguiente->x - posActual->x) * *Juego::getInstance()->getPercentTick() + posActual->x;
    float posFinY = (posSiguiente->y - posActual->y) * *Juego::getInstance()->getPercentTick() + posActual->y;
    sprite->setPosition(posFinX,posFinY);


    float nuevaVelocidad = ((posFinY - (float)POS_MAX_Y) / ((float)POS_MIN_Y - (float)POS_MAX_Y)) * ((float)VEL_MAX_Z - (float)VEL_MIN_Z) + (float)VEL_MIN_Z;
    if(nuevaVelocidad != Jugador::velocidadZ)
    {
        Jugador::velocidadZ = nuevaVelocidad;
        //cout << "La nueva velocidad es " << velocidadZ << endl;
    }


    // rotación
    float rotacionFin = (rotacionSiguiente - rotacionActual) * *Juego::getInstance()->getPercentTick() + rotacionActual;
    sprite->setRotation(rotacionFin);

    if(pintarSombra)
    {
        spriteSombra->setPosition(posFinX, POS_Y_SOMBRA);
        spriteSombra->setRotation(rotacionFin);

        // escala sombra
        float scaleSombra = getEscalaSombra();
        spriteSombra->setScale(scaleSombra, scaleSombra);

        Juego::getInstance()->getWindow()->draw(*spriteSombra);
    }
    Juego::getInstance()->getWindow()->draw(*sprite);


    for(unsigned int i = 0; i < balas->size(); i++)
        (*balas)[i]->render();
}

float Jugador::getEscalaSombra()
{
    float denominador1 = ESCALA_SOMBRA_MIN - ESCALA_SOMBRA_MAX;
    float numerador2 = (sprite->getPosition().y - POS_MAX_Y);
    float denominador2 = POS_MIN_Y - POS_MAX_Y;

    return (denominador1 * numerador2 + ESCALA_SOMBRA_MAX * denominador2) / denominador2;
}

void Jugador::update()
{
    mover();
    //cout << "hay balas en pantalla " << balas->size() << endl;
    for(unsigned int i = 0; i < balas->size(); i++)
    {
        Bala *balaActual = (*balas)[i];
        Vector2f posBala = balaActual->getSprite()->getPosition();
        // si la bala se ha salido de la pantalla..
        if(posBala.x < 0 || posBala.x > Constantes::TAMANYO_VENTANA_X
            || posBala.y < 0 || posBala.y > Constantes::TAMANYO_VENTANA_Y)
        {
            balas->erase(balas->begin() + i);
            delete balaActual;
        }
        else
            balaActual->update();
    }

}


void Jugador::mover()
{
    vectorDirector->x = 0.f;
    vectorDirector->y = 0.f;

    vectorDirectorOtros->x = 0.f;
    vectorDirectorOtros->y = 0.f;

    Vector2f *vectorDirectorBalas = new Vector2f(0, -1);

    Vector2f aux = *posSiguiente;

    // izq/dch y rotación
    float rotacionAux = rotacionSiguiente;
    if (Keyboard::isKeyPressed(Keyboard::Left)
        && !Keyboard::isKeyPressed(Keyboard::Right))
    {
        if(aux.x > MARGEN_LATERAL)
            vectorDirector->x = -1;
        else
            vectorDirectorOtros->x = 1;

        vectorDirectorBalas->x = -1;
        rotacionSiguiente = -GRADOS_GIRO;
    }
    else if (Keyboard::isKeyPressed(Keyboard::Right)
        && !Keyboard::isKeyPressed(Keyboard::Left))
    {
        if(aux.x < Constantes::TAMANYO_VENTANA_X - MARGEN_LATERAL)
            vectorDirector->x = 1;
        else
            vectorDirectorOtros->x = -1;

        vectorDirectorBalas->x = 1;
        rotacionSiguiente = GRADOS_GIRO;
    } else {
        rotacionSiguiente = 0;
    }
    rotacionActual = rotacionAux;
    // end rotación


    // arriba/abajo
    if (Keyboard::isKeyPressed(Keyboard::Up)
        && !Keyboard::isKeyPressed(Keyboard::Down))
    {
        if(aux.y > POS_MIN_Y)
            vectorDirector->y = -1;
        vectorDirectorBalas->y = -1;
    }
    else if (Keyboard::isKeyPressed(Keyboard::Down)
        && !Keyboard::isKeyPressed(Keyboard::Up))
    {
        if(aux.y < POS_MAX_Y)
            vectorDirector->y = 1;
        vectorDirectorBalas->y = 1;
    }

    if(Keyboard::isKeyPressed(Keyboard::S))
    {
        cout << "Hemos pulsado disparo" << endl;
        disparar(vectorDirectorBalas);
    }


    posSiguiente->x = posSiguiente->x + vectorDirector->x * velocidad;
    posSiguiente->y = posSiguiente->y + vectorDirector->y * velocidad;
    *posActual = aux;
    // end posición
}

void Jugador::objetivoConseguido()
{
    numObjetivos--;
    *puntuacion += Constantes::PUNTUACION_POR_TORRETA;
}


void Jugador::perderVida()
{
    if(!Juego::getInstance()->getModoDios())
        vidas--;
    if(vidas <= 0)
        Juego::getInstance()->setState(State::Estados::GAMEOVER);
}

void Jugador::ganarVida()
{
    if(vidas <= 10)
        vidas++;
}

void Jugador::disparar(Vector2f *vectorDirectorBalas)
{
    int tiempoEntreBalas = relojCadencia->getElapsedTime().asMilliseconds();
    if(tiempoEntreBalas > TIEMPO_ENTRE_BALAS)
    {
        relojCadencia->restart();
        Bala *bala = new Bala(sprite->getPosition(), vectorDirectorBalas);
        balas->push_back(bala);
    }
}

void Jugador::comprobarBalasContraEnemigos(vector<Enemigo *> *enemigos, bool sumarObjetivo, vector<Vida *> *vidas)
{
    for(unsigned int i = 0; i < enemigos->size(); i++)
    {
        Enemigo *enemigoActual = (*enemigos)[i];
        for(unsigned int j = 0; j < balas->size(); j++)
        {
            Bala *balaActual = (*balas)[j];
            if(enemigoActual->getSprite()->getGlobalBounds().intersects(balaActual->getSprite()->getGlobalBounds()))
            {
                enemigoActual->restarVida();
                if(enemigoActual->getVida() <= 0)
                {
                    int rand = Funciones::random(0, 100);
                    if(rand < PORCENTAJE_VIDA)
                    {
                        Vida *v = new Vida(0, enemigoActual->getSprite()->getPosition().x, enemigoActual->getSprite()->getPosition().y);
                        vidas->push_back(v);
                    }


                    *puntuacion += enemigoActual->getPuntuacion();
                    enemigos->erase(enemigos->begin()+i);
                    delete enemigoActual;
                }

                if(sumarObjetivo)
                    objetivoConseguido();
                balas->erase(balas->begin()+j);
                delete balaActual;
            }
        }
    }
}
